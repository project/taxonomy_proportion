AUTHOR
------
stred (http://drupal.org/user/956840)

DESCRIPTION
-----------
This module allows you to add a proportion to taxonomy terms when attached to an entity (node, user, ...). This helps you to weight the term used on a single basis without loosing the power of taxonomy and its core integration with vocabulary or term page, views, ...

2 widgets are available : autocomplete (with term creation) and radio/checkboxes
field prefix (like € or $) or suffix (like %) can be used

a formatter that allows you to display the term after or before the proportion. 

the module has been designed to work with views

use cases:
recipes: list of the ingredients with their proportion.
products: composition of the material like grape varieties in wine, metals in jewellery, ...
people: weight their strengths and weakness 

INSTALLATION
------------
Enable the module like any other Drupal Module /admin/build/modules.

USAGE
-----
Create a field "taxonomy proportion", choose an appropriate widget (checkboxes or autocomplete)
choose a vocabulary (just like regular taxonomy) 